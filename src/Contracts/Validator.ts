interface Validator<T> {

    validate(model: T): boolean;

    getErrors(): string[];

}