interface Repository<T extends Model> {

    getAll(callback: (data: T[]) => any);

    get(id: string, callback: (data: T) => any);

    insert(data: T, callback: (data: string) => any);

    update(data: T, callback: (data: boolean) => any);

    delete(data: string, callback: (data: boolean) => any);

    getOneByWhere(key: string, value: string, callback: (data: T) => any);

    getManyByWhere(key: string, value: string, callback: (data: T[]) => any);

}

interface Model {
    hash: string;
}