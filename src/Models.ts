/// <reference path="Contracts/Repository.ts" />
/// <reference path="Contracts/Validator.ts" />
/// <reference path="../def/validator/validator.d.ts" />
import validator = require('validator');

export class Task implements Model {

    public uid     : string;
    public hash    : string;
    public project : string;

    //Data
    public name     : string;
    public resume   : string;
    public end      : string;
    public state    : string;
    public estimate : number;

}

export class TaskValidator implements Validator<Task> {

    private isOk: boolean;
    private messages: string[];

    validate(model:Task):boolean {
        this.isOk = true;
        this.messages = [];

        if ('hash' in model && !validator.isAscii(model.hash)) {
            this.messages.push("Hash must be alphanumeric");
        }
        if (!validator.isAscii(model.project)) {
            this.messages.push("Project Hash must be alphanumeric");
        }
        if (!validator.isAscii(model.name)) {
            this.messages.push("Name must be alphanumeric");
        }
        if (!validator.isLength(model.name, 2)) {
            this.messages.push("Name is too short");
        }
        if (!validator.isLength(model.name, 2, 256)) {
            this.messages.push("Name is too long");
        }
        if ("resume" in model && !validator.isLength(model.resume, 0, 256)) {
            this.messages.push("Resume is too long");
        }
        if ('end' in model && model.end != "" && !validator.isNumeric(model.end)) {
            this.messages.push("End format is incorrect");
        }
        if ("estimate" in model && !validator.isNumeric('' + model.estimate)) {
            this.messages.push("Estimate format is incorrect");
        }
        if (!validator.isNumeric(model.state)) {
            this.messages.push("State format is incorrect");
        }
        //TODO check that state is one of X options

        this.isOk = this.messages.length == 0;
        return this.isOk;
    }

    getErrors(): string[] {
        return this.messages;
    }

}

export class Project implements Model {

    public hash: string;

    public name: string;

}

export class ProjectValidator implements Validator<Project> {

    private isOk: boolean;
    private messages: string[];

    validate(model:Project):boolean {
        this.isOk = true;
        this.messages = [];

        if ('hash' in model && !validator.isAscii(model.hash)) {
            this.messages.push("Hash must be alphanumeric");
        }
        if (!validator.isAlphanumeric(model.name)) {
            this.messages.push("Name must be alphanumeric");
        }
        if (!validator.isLength(model.name, 2, 256)) {
            this.messages.push("Name length is incorrect");
        }

        this.isOk = this.messages.length == 0;
        return this.isOk;
    }

    getErrors(): string[] {
        return this.messages;
    }

}

export class User implements Model {

    public hash: string;
    public name: string;
    public pass: string;
    public project: string;

}

export class UserValidator implements Validator<User> {

    private isOk: boolean;
    private messages: string[];

    validate(model:User):boolean {
        this.isOk = true;
        this.messages = [];

        if ('hash' in model && !validator.isAscii(model.hash)) {
            this.messages.push("Hash must be alphanumeric");
        }
        if (!validator.isAlphanumeric(model.name)) {
            this.messages.push("Name must be alphanumeric");
        }
        if (!validator.isLength(model.name, 2, 256)) {
            this.messages.push("Name length is incorrect");
        }

        this.isOk = this.messages.length == 0;
        return this.isOk;
    }

    getErrors(): string[] {
        return this.messages;
    }

}