/// <reference path="../def/express/express.d.ts" />

import Express       = require('express');
var Ejs: any         = require('ejs');

import Configuration = require('./Configuration');
import Repositories  = require('./Repositories');
import Models        = require('./Models');

export class Public {

    private config: Configuration.Config;
    private app   : Express.Application;

    constructor(config: Configuration.Config, app: Express.Application) {
        this.config = config;
        this.app    = app;

        if (config.get("functions.enable_public_frontend")) {
            //TODO Setup public routes
            console.log("Public front enabled");
            //Setup rendering engine
            app.engine('.ejs', Ejs.__express);
            app.set('views', 'public');
            app.set('view engine', 'ejs');
            app.use(Express.static('public'));

            app.post('/contact', function(req: Express.Request, res: Express.Response, next) {

            });

            app.get('/', function(req, res, next) {
                res.render('index.ejs')
            });
            app.get('/:page', function(req, res, next) {
                try {
                    res.render(req.params.page + ".ejs")
                } catch(e) {
                    res.sendStatus(404);
                }
            });
        }
    }

}

export class Admin {

    private config: Configuration.Config;
    private app   : Express.Application;

    constructor(config: Configuration.Config, app: Express.Application) {
        this.config = config;
        this.app    = app;
    }

}