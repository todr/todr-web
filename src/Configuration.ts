/// <reference path="../def/node/node.d.ts" />
/// <reference path="../def/convict/convict.d.ts" />
import fs = require('fs');
import convict = require('convict');

export var configValidator = <any>{
    env: <any>{
        doc: 'The application environement.',
        format: ['production', 'development', 'test'],
        default: 'development',
        env: 'TODR_ENV',
        arg: 'env'
    },
    ip: <any>{
        doc: 'The application ip to bind on',
        format: 'ipaddress',
        default: '0.0.0.0',
        env: 'TODR_IP',
        arg: 'ip'
    },
    port: <any>{
        doc: 'The application port to run on',
        format: 'port',
        default: 8080,
        env: 'TODR_PORT',
        arg: 'port'
    },
    secret: <any>{
        doc: 'The application secret to generate token',
        format: String,
        default: 'IAmNotVerySercret',
        env: 'TODR_SECRET',
        arg: 'secret'
    },
    database: {
        type: {
            doc: 'The type of database',
            format: String,
            default: "file",
            env: 'TODR_DATABASE_TYPE'
        },
        path: {
            doc: 'The path to the file database',
            format: String,
            default: "./data.db",
            env: 'TODR_DATABASE_FILE'
        },
        name: {
            doc: 'The database name',
            format: String,
            default: "todr",
            env: 'TODR_DATABASE_NAME'
        },
        host: {
            doc: 'The database host',
            format: String,
            default: "127.0.0.1",
            env: 'TODR_DATABASE_HOST'
        },
        port: {
            doc: 'The database port',
            format: String,
            default: "",
            env: 'TODR_DATABASE_PORT'
        },
        username: {
            doc: 'The database username',
            format: String,
            default: "",
            env: 'TODR_DATABASE_USERNAME'
        },
        password: {
            doc: 'The database password',
            format: String,
            default: "",
            env: 'TODR_DATABASE_PASSWORD'
        }
    },
    rollbar_token: <any>{
        doc: 'The rollbar to report to',
        format: String,
        default: null,
        env: 'TODR_ROLLBAR_TOKEN'
    },
    mailgun_token: <any>{
        doc: 'The mailgun to use for outgoing emails',
        format: String,
        default: null,
        env: 'TODR_MAILGUN_TOKEN'
    },
    functions: <any>{
        enable_public_frontend: {
            doc: 'Enable the public frontend (ie: The todr website)',
            format: Boolean,
            default: false,
            env: 'TODR_PUBLIC_FRONTEND'
        },
        enable_admin_frontend: {
            doc: 'Enable the admin frontend (ie: The admin panel)',
            format: Boolean,
            default: true,
            env: 'TODR_ADMIN_FRONTEND'
        }
    }
};

export var defaultConfig: Config = null;

export function getDefaultConfig(): Config {
    if (defaultConfig == null) {
        defaultConfig = new Config();
        defaultConfig.loadFile("./config.json");
        defaultConfig.loadFile("./config."+defaultConfig.get('env')+".json");
    }

    return defaultConfig;
}

export class Config {

    private config;

    public constructor() {
        this.config = convict(configValidator);
    }

    public loadFile(path: string) {
        if (fs.existsSync(path)) {
            console.log('Loading configuration from '+path);
            this.config.loadFile(path);
        } else {
            console.log('File '+path+' doesn\'t exists, ignoring');
        }
    }

    public validate(): boolean {
        return this.config.validate();
    }

    public get(name: string) {
        return this.config.get(name);
    }

}