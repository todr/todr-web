/// <reference path="../def/mongodb/mongodb.d.ts" />
import Configuration = require('./Configuration');
import Mongo = require("mongodb");
var    NeDB  : any = require('nedb');

export class DataDriver {

    private static instance: DataDriver;

    public static prepare(config: Configuration.Config) {
        if (config.get('database.type') == "file") {
            DataDriver.instance = new FileDriver(config);
        } else if (config.get('database.type') == "mongo") {
            DataDriver.instance = new MongoDriver(config);
        }
    }

    public static close() {
        DataDriver.instance.close();
    }

    public static getInstance(): DataDriver {
        return DataDriver.instance;
    }

    close() {}

    getAll(type: string, callback: (err: Error, docs: any[]) => void):void {}

    get(type: string, id: string, callback: (err: Error, docs: any[]) => void) {}

    insert(type: string, data: any, callback: (err: Error, data: string) => any) {}

    update(type: string, data: any, callback: (err: Error, count: number, newDoc: any) => any) {}

    remove(type: string, id: string, callback: (err: Error, count: number) => any) {}

    getOneByWhere(type: string, key: string, value: string, callback: (err: Error, doc: any[]) => any) {}

    getManyByWhere(type: string, key: string, value: string, callback: (err: Error, docs: any[]) => any) {}

    getOneByWhereAnd(type: string, keyAndValues: any, callback: (err: Error, doc: any[]) => any) {}

}

export class FileDriver extends DataDriver {

    private db;

    public constructor(config: Configuration.Config) {
        super();
        this.db = new NeDB({ filename: config.get('database.path'), autoload: true });
    }

    public close() {
        this.db.close();
    }

    public getAll(type: string, callback: (err: Error, docs: any[]) => void):void {
        this.db.find({type: type}, (err: Error, docs: any[]) => {
            docs = this.prepareDocs(docs);

            callback(err, docs);
        });
    }

    public get(type: string, id: string, callback: (err: Error, docs: any[]) => void) {
        this.db.findOne({_id: id, type: type}, (err: Error, doc: any) => {
            doc = this.prepareDoc(doc);

            callback(err, doc);
        });
    }

    public insert(type: string, data: any, callback: (err: Error, data: string) => any) {
        (<any>data).type = type;
        this.db.insert(data, (err: Error, newDoc: any) => {
            newDoc = this.prepareDoc(newDoc);

            callback(err, newDoc);
        });
    }

    public update(type: string, data: any, callback: (err: Error, count: number, newDoc: any) => any) {
        var id = data.hash;
        delete data.hash;
        (<any>data).type = type;
        this.db.update({_id: id, type: type}, data, (err: Error, count: number, newDoc: any) => {
            newDoc = this.prepareDoc(newDoc);

            callback(err, count, newDoc);
        });
    }

    public remove(type: string, id: string, callback: (err: Error, count: number) => any) {
        this.db.remove({_id: id, type: type}, {}, function (err: Error, count: number) {
            callback(err, count);
        });
    }

    public getOneByWhere(type: string, key: string, value: string, callback: (err: Error, doc: any) => any) {
        var selector = {type: type};
        selector[key] = value;

        this.db.findOne(selector, (err: Error, doc: any) => {
            doc = this.prepareDoc(doc);

            callback(err, doc);
        });
    }

    public getManyByWhere(type: string, key: string, value: string, callback: (err: Error, docs: any[]) => any) {
        var selector = {type: type};
        selector[key] = value;

        this.db.find(selector, (err: Error, docs: any[]) => {
            docs = this.prepareDocs(docs);

            callback(err, docs);
        });
    }

    public getOneByWhereAnd(type: string, keyAndValues: any, callback: (err: Error, doc: any[]) => any) {
        var selector = {type: type};
        for (var key in keyAndValues) {
            selector[key] = keyAndValues[key];
        }

        this.db.find(selector, (err: Error, docs: any[]) => {
            docs = this.prepareDocs(docs);

            callback(err, docs);
        });
    }

    private prepareDoc(doc: any) {
        if (doc != null && '_id' in doc) {
            doc.hash = doc._id;
            delete doc._id;
        }

        return doc;
    }

    private prepareDocs(docs: any[]) {
        for (var i = 0; i < docs.length; i++) {
            docs[i] = this.prepareDoc(docs[i]);
        }

        return docs;
    }

}

export class MongoDriver extends DataDriver {

    private static config: Configuration.Config;

    private static db: Mongo.Db = null;
    private static dbReady: boolean = false;

    public constructor(config: Configuration.Config) {
        super();

        MongoDriver.config = config;
    }

    public close() {
        MongoDriver.db.close();
    }

    getAll(type: string, callback: (err: Error, docs: any[]) => void):void {
        this.ensureConnection(() => {
            var collection = MongoDriver.db.collection(type);
            collection.find({}).toArray((err:Error, docs:any []) => {
                docs = this.prepareDocs(docs);
                callback(err, docs);
            });
        });
    }

    get(type: string, id: string, callback: (err: Error, doc: any[]) => void) {
        this.ensureConnection(() => {
            try {
                var collection = MongoDriver.db.collection(type);
                collection.findOne({_id: new Mongo.ObjectID(id)}, (err:Error, result: any) => {
                    var doc = this.prepareDoc(result);
                    callback(err, doc);
                });
            } catch (err) {
                //TODO log error
                callback(err, null);
            }
        });
    }

    insert(type: string, data: any, callback: (err: Error, data: string) => any) {
        this.ensureConnection(() => {
            var collection = MongoDriver.db.collection(type);
            collection.insert(data, {safe:true}, (err: Error, result: any[]) => {
                collection.find().sort({_id:-1}).limit(1).toArray((err: Error, result: any) => {
                    if (err) {
                        throw err;
                    }

                    if (result.length != 1) {
                        throw new Error ("Incosistent state?")
                    }

                    var newDoc = this.prepareDoc(result[0]);
                    callback(err, newDoc);
                });
            });
        });
    }

    update(type: string, data: any, callback: (err: Error, count: number, newDoc: any) => any) {
        this.ensureConnection(() => {
            try {
                var collection = MongoDriver.db.collection(type);

                var id = data.hash;
                delete data.hash;

                var sort = [];
                sort['_id'] = 'descending';

                collection.findAndModify({_id: new Mongo.ObjectID(id)}, sort, data, {}, (err: Error, result: any) => {
                    var newDoc = this.prepareDoc(result);

                    callback(err, 1, newDoc);
                });
            } catch (err) {
                //TODO log error
                callback(err, 0, null);
            }
        });
    }

    remove(type: string, id: string, callback: (err: Error, count: number) => any) {
        this.ensureConnection(() => {
            try {
                var collection = MongoDriver.db.collection(type);

                collection.remove({_id: new Mongo.ObjectID(id)}, {}, function (err: Error, result) {
                    //TODO improve that?
                    callback(err, 1);
                });
            } catch (err) {
                //TODO log error
                callback(err, 0);
            }
        });
    }

    getOneByWhere(type: string, key: string, value: string, callback: (err: Error, doc: any[]) => any) {
        this.ensureConnection(() => {
            var collection = MongoDriver.db.collection(type);
            var selector = {};
            selector[key] = value;
            collection.findOne(selector, (err:Error, result: any) => {
                var doc = this.prepareDoc(result);
                callback(err, doc);
            });
        });
    }

    getManyByWhere(type: string, key: string, value: string, callback: (err: Error, docs: any[]) => any) {
        this.ensureConnection(() => {
            var collection = MongoDriver.db.collection(type);
            var selector = {};
            selector[key] = value;
            collection.find(selector).toArray((err:Error, result: any[]) => {
                var docs = this.prepareDocs(result);
                callback(err, docs);
            });
        });
    }

    getOneByWhereAnd(type: string, keyAndValues: any, callback: (err: Error, doc: any[]) => any) {
        this.ensureConnection(() => {
            var collection = MongoDriver.db.collection(type);
            var selector = {};
            for (var key in keyAndValues) {
                selector[key] = keyAndValues[key];
            }

            collection.findOne(selector, (err:Error, result: any) => {
                var doc = this.prepareDoc(result);
                callback(err, doc);
            });
        });
    }

    private ensureConnection(callback: () => void) {
        //No connection in place? Create a new
        if (MongoDriver.db == null) {
            //Create a new one
            var name = MongoDriver.config.get('database.name');
            var host = MongoDriver.config.get('database.host');
            var port = MongoDriver.config.get('database.port');
            var user = MongoDriver.config.get('database.username');
            var pass = MongoDriver.config.get('database.password');
            var db = new Mongo.Db(name, new Mongo.Server(host, port, {auto_reconnect: false, poolSize: 4}), {w:0, native_parser: false});

            MongoDriver.dbReady = false;
            MongoDriver.db = db;

            db.open(function(err, db) {
                if (db) {
                    MongoDriver.db = db;
                    db.authenticate(user, pass, function(err, result) {
                        MongoDriver.db = db;
                        MongoDriver.dbReady = true;
                        console.log("Successfully connected to mongoDB storage");
                    });
                } else {
                    throw new Error("Can't connect to mongoDB storage");
                }
            });
        }

        if (MongoDriver.dbReady) {      //If the connection is ready, carry it
            callback();
        } else {                        //Otherwise, setTimeout to carry it later
            setTimeout(() => {
                this.ensureConnection(callback);
            }, 5);
        }
    }

    private prepareDoc(doc: any) {
        if (doc != null && '_id' in doc) {
            doc.hash = doc._id;
            delete doc._id;

            //Autohide some fields
            if ('pass' in doc) {
                delete doc.pass;
            }
            if ('password' in doc) {
                delete doc.password;
            }
        }

        return doc;
    }

    private prepareDocs(docs: any[]) {
        if (docs == null) {
            return [];
        }

        for (var i = 0; i < docs.length; i++) {
            docs[i] = this.prepareDoc(docs[i]);
        }

        return docs;
    }

}