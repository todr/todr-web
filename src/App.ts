/// <reference path="../def/express/express.d.ts" />
/// <reference path="../def/node/node.d.ts" />
/// <reference path="../def/body-parser/body-parser.d.ts" />
import Express       = require('express');
import BodyParser    = require('body-parser')
import Configuration = require('./Configuration');
import DataLayer     = require('./DataLayer');
import Repositories  = require('./Repositories');
import Models        = require('./Models');
import Frontend      = require('./Frontend');
import Api           = require('./Api');
import Auth          = require('./Auth');

//Load the configuration and pre_start the app
var config: Configuration.Config = Configuration.getDefaultConfig();
console.log("Application set env to "+config.get('env'));

DataLayer.DataDriver.prepare(config);

var app = Express();            //Start the app
app.use(BodyParser.json());     //Setup json body handler

//Setup error reporting
if (config.get("rollbar_token") != null) {
    var rollbar: any = require("rollbar");
    app.use(rollbar.errorHandler(config.get("rollbar_token")));
    rollbar.handleUncaughtExceptions(config.get("rollbar_token"),  { exitOnUncaughtException: true });
    console.log("Setuped rollbar logger");
}

//Setup auth
//Auth route is defined before auth check
//All route defined after this point need auth to work
var auth = new Auth.ApiAuth(config, app);
app.post('/apiv1/:prjHash/auth', auth.authRoute);
app.use('/apiv1/', auth.middleware);

//Setup public endpoint handlers
var frontendPublic = new Frontend.Public(config, app);
var frontendAdmin  = new Frontend.Admin(config, app);

//Setup endpoints api handlers
var apiProjects = new Api.ApiProjects(config, app);
var apiTask     = new Api.ApiTasks(config, app);
var apiUsers    = new Api.ApiUsers(config, app);

//Start the server to handle requests
app.listen(config.get('port'), config.get('ip'), () => {
    console.log("Application started on "+config.get('ip')+":"+config.get('port'));
});

