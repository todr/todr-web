/// <reference path="../def/express/express.d.ts" />
import Express       = require('express');
import Configuration = require('./Configuration');
import Repositories  = require('./Repositories');
import Models        = require('./Models');
var    Jwt:any       = require('jsonwebtoken');

export class ApiAuth {

    private config: Configuration.Config;
    private app: Express.Application;

    private secret: string;

    public constructor(config: Configuration.Config, app: Express.Application) {
        this.config = config;
        this.app    = app;
    }

    auth = (project: string, user: string, pass: string, callback: (err: Error, token: string) => void) => {
        var repo = new Repositories.UserRepository(this.config);
        var selector = {
            "project": project,
            "name":user,
            "pass":pass
        };
        repo.getOneByWhereAnd(selector, (dataUser : Models.User) => {
            if (dataUser == null) {
                callback(new Error("Invalid login"), null);
            } else {
                var data = {
                    "project": dataUser.project,
                    "user"   : dataUser.hash
                };
                callback(null, Jwt.sign(data, this.config.get("secret"), { expiresInMinutes: 1440 /* Expires in 24h */ }));
            }
        })
    };

    authRoute = (req: Express.Request, res: Express.Response) => {
        if (!('user' in req.body) || !('pass' in req.body)) {
            return res.status(400).json(['No credentials submitted']);
        }

        var prjt = req.params.prjHash;
        var user = req.body.user;
        var pass = req.body.pass;

        this.auth(prjt, user, pass, (err: Error, token: string) => {
            if (err) {
                return res.status(403).json([err.message]);
            }

            res.status(200).json({"token": token});
        });
    };

    middleware = (req: Express.Request, res: Express.Response, next: any): void => {
        //check that json body contains token
        //      Or query
        //      Or headers
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        if (token) {
            //Root login
            if (token == this.config.get("secret")) {
                (<any>req).isAuth = true;
                (<any>req).isAdmin = true;
                (<any>req).user = "admin";
                return next();
            }

            //verify that token is good
            Jwt.verify(token, this.config.get("secret"), function(err, decoded) {
                if (err) {
                    res.status(403).json(["Failed to authenticate token"]);
                } else {
                    // if everything is good, save to request for use in other routes
                    (<any>req).isAuth = true;
                    (<any>req).isAdmin = false;
                    (<any>req).user = decoded.user;
                    (<any>req).project = decoded.project;
                    return next();
                }
            });
        } else {
            res.status(401).json(["No token provided"]);
        }

    };

}