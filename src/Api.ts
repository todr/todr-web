/// <reference path="../def/express/express.d.ts" />
import Express       = require('express');
import Configuration = require('./Configuration');
import Repositories  = require('./Repositories');
import Models        = require('./Models');

export class ApiProjects {

    private config: Configuration.Config;
    private app: Express.Application;

    constructor(config: Configuration.Config, app: Express.Application) {
        this.config = config;
        this.app    = app;

        //Setup project level handlers
        app.get   ('/apiv1/', this.getIndex);
        app.post  ('/apiv1/', this.postProject);
        app.get   ('/apiv1/:prjHash', this.getProject);
        app.post  ('/apiv1/:prjHash', this.updateProject);
        app.delete('/apiv1/:prjHash', this.deleteProject);
    }

    getIndex = (req: Express.Request, res: Express.Response) => {
        var repo = new Repositories.ProjectRepository(this.config);

        repo.getAll((projects : Models.Project[]) => {
            res.json(projects);
        })
    };

    postProject = (req: Express.Request, res: Express.Response) => {
        var repo = new Repositories.ProjectRepository(this.config);
        var project = req.body;

        var validator = new Models.ProjectValidator();
        if (!validator.validate(project)) {
            res.status(400).json(validator.getErrors());
        } else {
            repo.insert(project, (hash: string) => {
                res.json({hash: hash});
            });
        }
    };

    getProject = (req: Express.Request, res: Express.Response) => {
        var repoProjt = new Repositories.ProjectRepository(this.config);
        var repoUsers = new Repositories.UserRepository(this.config);
        var projectHash = req.params.prjHash;

        repoProjt.get(projectHash, (project : Models.Project) => {
            if (project == null) {
                res.sendStatus(404);
            } else {
                repoUsers.getManyByWhere("project", req.params.prjHash, (users : Models.User[]) => {
                    (<any>project).users = users;
                    res.json(project);
                })
            }
        });
    };

    updateProject= (req: Express.Request, res: Express.Response) => {
        var repo = new Repositories.ProjectRepository(this.config);
        var project = req.body;
        project.hash = req.params.prjHash;

        var validator = new Models.ProjectValidator();
        if (!validator.validate(project)) {
            res.status(400).json(validator.getErrors());
        } else {
            repo.update(project, (done: boolean) => {
                res.json({done: done});
            });
        }
    };

    deleteProject = (req: Express.Request, res: Express.Response) => {
        var repoProject = new Repositories.ProjectRepository(this.config);
        var repoTask    = new Repositories.TaskRepository(this.config);
        var projectHash = req.params.prjHash;

        repoProject.get(projectHash, (data: Models.Project) => {
            if (data == null) {
                res.sendStatus(404);
            } else {
                repoTask.getManyByWhere("project", projectHash, (tasks: Models.Task[]) => {
                    if (tasks.length == 0) {
                        repoProject.delete(projectHash, (done: boolean) => {
                            res.json({done: done});
                        });
                    } else {
                        res.status(400).json(["Project has tasks"]);
                    }
                });
            }
        });
    };

}

export class ApiTasks {

    private config: Configuration.Config;
    private app: Express.Application;

    constructor(config: Configuration.Config, app: Express.Application) {
        this.config = config;
        this.app    = app;

        //Setup project level handlers
        app.get   ('/apiv1/:prjHash/tasks', this.getTasks);
        app.post  ('/apiv1/:prjHash/tasks', this.postTask);
        app.get   ('/apiv1/:prjHash/tasks/:taskHash', this.getTask);
        app.post  ('/apiv1/:prjHash/tasks/:taskHash', this.updateTask);
        app.delete('/apiv1/:prjHash/tasks/:taskHash', this.deleteTask);
    }

    getTasks = (req: Express.Request, res: Express.Response) => {
        var repo = new Repositories.TaskRepository(this.config);

        repo.getManyByWhere("project", req.params.prjHash, (tasks : Models.Task[]) => {
            res.json(tasks);
        })
    };

    postTask = (req: Express.Request, res: Express.Response) => {
        var repo = new Repositories.TaskRepository(this.config);
        var task = req.body;
        task.project = req.params.prjHash;

        //Validate the task
        var validator = new Models.TaskValidator();
        if (!validator.validate(task)) {
            res.status(400).json(validator.getErrors());
        } else {
            repo.getOneByWhere("uid", task.uid, (duplicateTask: Models.Task) => {
                if (duplicateTask == null) {
                    repo.insert(task, (hash: string) => {
                        res.json({"hash": hash});
                    });
                } else {
                    res.status(304).json(["Task already saved"]);
                }
            });
        }
    };

    getTask = (req: Express.Request, res: Express.Response) => {
        var repo = new Repositories.TaskRepository(this.config);

        repo.get(req.params.taskHash, (task: Models.Task) => {
            if (task != null && task.project == req.params.prjHash) {
                res.json(task);
            } else {
                res.sendStatus(404);
            }
        })
    };

    updateTask = (req: Express.Request, res: Express.Response) => {
        var repo = new Repositories.TaskRepository(this.config);

        //fetch the task to check if project hash is ok
        repo.get(req.params.taskHash, (task: Models.Task) => {
            if (task != null && task.project == req.params.prjHash) {
                //Update the task
                task = req.body;
                task.hash = req.params.taskHash;
                task.project = req.params.prjHash;

                //Validate the task
                var validator = new Models.TaskValidator();
                if (!validator.validate(task)) {
                    return res.status(400).json(validator.getErrors());
                }

                repo.update(task, (done: boolean) => {
                    res.json({done: done});
                });
            } else {
                res.sendStatus(404);
            }
        })
    };

    deleteTask = (req: Express.Request, res: Express.Response) => {
        var repo = new Repositories.TaskRepository(this.config);

        //fetch the task to check if project hash is ok
        repo.get(req.params.taskHash, (task: Models.Task) => {
            if (task != null && task.project == req.params.prjHash) {
                repo.delete(task.hash, (done: boolean) => {
                    res.json({done: done});
                });
            } else {
                res.sendStatus(404);
            }
        })
    };

}

export class ApiUsers {

    private config:Configuration.Config;
    private app   :Express.Application;

    constructor(config:Configuration.Config, app:Express.Application) {
        this.config = config;
        this.app = app;

        //Setup project level handlers
        app.get   ('/apiv1/:prjHash/users', this.getUsers);
        app.post  ('/apiv1/:prjHash/users', this.postUser);
        app.get   ('/apiv1/:prjHash/users/:userHash', this.getUser);
        app.post  ('/apiv1/:prjHash/users/:userHash', this.updateUser);
        app.delete('/apiv1/:prjHash/users/:userHash', this.deleteUser);
    }

    getUsers = (req: Express.Request, res: Express.Response) => {
        var repo = new Repositories.UserRepository(this.config);

        repo.getManyByWhere("project", req.params.prjHash, (users : Models.User[]) => {
            res.json(users);
        })
    };

    postUser = (req: Express.Request, res: Express.Response) => {
        var repo = new Repositories.UserRepository(this.config);
        var user = req.body;
        user.project = req.params.prjHash;

        //Validate the task
        var validator = new Models.UserValidator();
        if (!validator.validate(user)) {
            res.status(400).json(validator.getErrors());
        } else {
            repo.insert(user, (hash: string) => {
                res.json({"hash": hash});
            });
        }
    };

    getUser = (req: Express.Request, res: Express.Response) => {
        var repo = new Repositories.UserRepository(this.config);

        repo.get(req.params.userHash, (user: Models.User) => {
            if (user != null && user.project == req.params.prjHash) {
                res.json(user);
            } else {
                res.sendStatus(404);
            }
        })
    };

    updateUser = (req: Express.Request, res: Express.Response) => {
        var repo = new Repositories.UserRepository(this.config);

        //fetch the task to check if project hash is ok
        repo.get(req.params.taskHash, (user: Models.User) => {
            if (user.project == req.params.prjHash) {
                //Update the task
                user = req.body;
                user.hash = req.params.taskHash;
                user.project = req.params.prjHash;

                //Validate the task
                var validator = new Models.UserValidator();
                if (!validator.validate(user)) {
                    return res.status(400).json(validator.getErrors());
                }

                repo.update(user, (done: boolean) => {
                    res.json({done: done});
                });
            } else {
                res.sendStatus(404);
            }
        })
    };

    deleteUser = (req: Express.Request, res: Express.Response) => {
        var repo = new Repositories.UserRepository(this.config);

        //fetch the task to check if project hash is ok
        repo.get(req.params.taskHash, (user: Models.User) => {
            if (user != null && user.project == req.params.prjHash) {
                repo.delete(user.hash, (done: boolean) => {
                    res.json({done: done});
                });
            } else {
                res.sendStatus(404);
            }
        })
    };

}