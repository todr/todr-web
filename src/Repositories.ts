/// <reference path="Contracts/Repository.ts" />
import Configuration = require('./Configuration');
import Models        = require('./Models');
import DataLayer     = require('./DataLayer');

export class BaseRepository<T extends Model> implements Repository<T> {

    private type: string;
    private driver: DataLayer.DataDriver;

    public constructor(config: Configuration.Config, type: string) {
        this.type = type;

        this.driver = DataLayer.DataDriver.getInstance();
    }

    public getAll(callback: (data: T[]) => any) {
        this.driver.getAll(this.type, (err: Error, docs: any[]) => {
            if (err) {
                throw err;
            }

            callback(docs);
        });
    }

    public get(id: string, callback: (data: T) => any) {
        this.driver.get(this.type, id, (err: Error, doc: any) => {
            if (err) {
                throw err;
            }

            callback(doc);
        });
    }

    public insert(data: T, callback: (data: string) => any) {
        this.driver.insert(this.type, data, (err: Error, newDoc: any) => {
            if (err) {
                throw err;
            }

            callback(newDoc.hash);
        });
    }

    public update(data: T, callback: (data: boolean) => any) {
        this.driver.update(this.type, data, (err: Error, count: number, newDoc: any) => {
            if (err) {
                throw err;
            }

            if (count ==  1) {
                callback(true)
            } else {
                callback(false)
            }
        });
    }

    public delete(hash: string, callback: (data: boolean) => any) {
        this.driver.remove(this.type, hash, function (err: Error, count: number) {
            if (err) {
                throw err;
            }

            if (count == 1) {
                callback(true)
            } else {
                callback(false)
            }
        })
    }

    public getOneByWhere(key: string, value: string, callback: (data: T) => any) {
        this.driver.getOneByWhere(this.type, key, value, (err: Error, doc: any) => {
            if (err) {
                throw err;
            }

            callback(doc);
        });
    }

    public getManyByWhere(key: string, value: string, callback: (data: T[]) => any) {
        this.driver.getManyByWhere(this.type, key, value, (err: Error, docs: any[]) => {
            if (err) {
                throw err;
            }

            callback(docs);
        });
    }

    public getOneByWhereAnd(keyAndValues: any, callback: (data: T) => any) {
        this.driver.getOneByWhereAnd(this.type, keyAndValues, (err: Error, doc: any) => {
            if (err) {
                throw err;
            }

            callback(doc);
        });
    }

}

export class TaskRepository extends BaseRepository<Models.Task> {

    public constructor(config: Configuration.Config) {
        super(config, "tasks");
    }

}

export class ProjectRepository extends BaseRepository<Models.Project> {

    public constructor(config: Configuration.Config) {
        super(config, "project");
    }

}

export class UserRepository extends BaseRepository<Models.Project> {

    public constructor(config: Configuration.Config) {
        super(config, "users");
    }

}