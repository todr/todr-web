/// <reference path="../def/mocha/mocha.d.ts" />
/// <reference path="../def/chai/chai.d.ts" />
/// <reference path="../def/supertest/supertest.d.ts" />
/// <reference path="../def/superagent/superagent.d.ts" />
import chai = require('chai');
import supertest = require('supertest');
var api: supertest.SuperTest = supertest('http://todr.atrakeur.com');
//var api: supertest.SuperTest = supertest('localhost:8080');
var expect = chai.expect;

describe("Tasks", function() {

    var projectHash;
    var taskHash;

    var projectName = "AutomaticTestProject";

    var incorrectData = {
        name: "a",
        resume: "",
        state: "TODO",
        estimate: "abc"
    };

    var taskData = {
        name: "SomeTestTask",
        resume: "A test task used by unit testing for testing...",
        state: "TODO",
        estimate: "60"
    };

    it('Should create a new project to prepare for tasks', function(done) {
        api.post('/')
            .send({name: projectName})
            .set('Accept', 'application/json')
            .expect(200, done)
            .expect('Content-Type', /json/)
            .expect((res:any) => {
                expect(res.body).have.property('hash').and.be.a('string');
                projectHash = res.body.hash;
            });
    });

    it('Should return an empty array for new project', function(done) {
        api.get('/'+projectHash+'/tasks')
            .set('Accept', 'application/json')
            .expect(200, done)
            .expect('Content-Type', /json/)
            .expect((res: any) => {
                expect(res.body).have.property('length').and.be.a('number').and.be.equal(0);
                expect(res.body).to.be.a('array');
            });
    });

    it('Should not accept to add a new incorrect task', function(done) {
        api.post('/'+projectHash+'/tasks')
            .send(incorrectData)
            .set('Accept', 'application/json')
            .expect(400, done)
            .expect('Content-Type', /json/)
            .expect((res: any) => {
                expect(res.body).is.a('array');
            });
    });

    it('Should accept to add a new correct task', function(done) {
        api.post('/'+projectHash+'/tasks')
            .send(taskData)
            .set('Accept', 'application/json')
            .expect(200, done)
            .expect('Content-Type', /json/)
            .expect((res: any) => {
                expect(res.body).have.property('hash');
                expect(res.body.hash).is.a('string');
                taskHash = res.body.hash;
            });
    });

    it('Should not accept to delete an non empty project', function(done) {
        api.del('/'+projectHash)
            .set('Accept', 'application/json')
            .expect(400, done);
    });

    it('Should accept to delete the new task', function(done) {
        api.del('/'+projectHash+'/tasks/'+taskHash)
            .set('Accept', 'application/json')
            .expect(200, done)
            .expect('Content-Type', /json/)
            .expect((res: any) => {
                expect(res.body).have.property('done');
                expect(res.body.done).is.ok;
            });
    });

    it('Should accept to delete the empty project', function(done) {
        api.del('/'+projectHash)
            .set('Accept', 'application/json')
            .expect(200, done)
            .expect('Content-Type', /json/)
            .expect((res: any) => {
                expect(res.body).have.property('done');
                expect(res.body.done).is.ok;
            });
    });

});