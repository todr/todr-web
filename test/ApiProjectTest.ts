/// <reference path="../def/mocha/mocha.d.ts" />
/// <reference path="../def/chai/chai.d.ts" />
/// <reference path="../def/supertest/supertest.d.ts" />
/// <reference path="../def/superagent/superagent.d.ts" />
import chai = require('chai');
import supertest = require('supertest');
var api: supertest.SuperTest = supertest('http://todr.atrakeur.com');
//var api: supertest.SuperTest = supertest('localhost:8080');
var expect = chai.expect;

describe("Projects", function() {

    var projectCount;
    var projectHash;
    var projectName = "AutomaticTestProject";
    var projectNameBis = "AutomaticTestProject2";

    it('Should return an array of all projects', function(done) {
        api.get('/')
            .set('Accept', 'application/json')
            .expect(200, done)
            .expect('Content-Type', /json/)
            .expect((res: any) => {
                expect(res.body).have.property('length').and.be.a('number');
                expect(res.body).to.be.a('array');
                projectCount = res.body.length;
            });
    });

    it('Should accept to create a project', function(done) {
        api.post('/')
            .send({name: projectName})
            .set('Accept', 'application/json')
            .expect(200, done)
            .expect('Content-Type', /json/)
            .expect((res: any) => {
                expect(res.body).have.property('hash').and.be.a('string');
                projectHash = res.body.hash;
            });
    });

    it('Should have created the project', function(done) {
        api.get('/')
            .set('Accept', 'application/json')
            .expect(200, done)
            .expect('Content-Type', /json/)
            .expect((res: any) => {
                expect(res.body.length).to.be.greaterThan(projectCount);
            });
    });

    it('Should accept to get a project', function(done) {
        api.get('/'+projectHash)
            .set('Accept', 'application/json')
            .expect(200, done)
            .expect('Content-Type', /json/)
            .expect((res: any) => {
                expect(res.body).have.property('hash').and.be.a('string').and.be.equal(projectHash);
                expect(res.body).have.property('name').and.be.a('string').and.be.equal(projectName);
            });
    });

    it('Should accept to update a project', function(done) {
        api.post('/'+projectHash)
            .send({name: projectNameBis})
            .set('Accept', 'application/json')
            .expect(200, done)
            .expect('Content-Type', /json/)
            .expect((res: any) => {
                expect(res.body).have.property('done').and.be.ok;
            });
    });

    it('Should return the updated project', function(done) {
        api.get('/'+projectHash)
            .set('Accept', 'application/json')
            .expect(200, done)
            .expect('Content-Type', /json/)
            .expect((res: any) => {
                expect(res.body).have.property('name');
                expect(res.body.name).is.equal(projectNameBis);
            });
    });

    it('Should accept to delete a project', function(done) {
        api.del('/'+projectHash)
            .set('Accept', 'application/json')
            .expect(200, done)
            .expect('Content-Type', /json/)
            .expect((res: any) => {
                expect(res.body).have.property('done');
                expect(res.body.done).is.ok;
            });
    });

    it('Should return a 404 on deleted project', function(done) {
        api.get('/'+projectHash)
            .set('Accept', 'application/json')
            .expect(404, done);
    });

    it('Should return the list without deleted project', function(done) {
        api.get('/')
            .set('Accept', 'application/json')
            .expect(200, done)
            .expect('Content-Type', /json/)
            .expect((res: any) => {
                expect(res.body).have.property('length').and.be.a('number');
                expect(res.body).to.be.a('array');
                expect(res.body.length).to.be.equal(projectCount);
            });
    });
});