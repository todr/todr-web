#Start the webserver
node build/App.js &
node_pid=$!

#WARN: This can fail if the server takes too long to startup
sleep 4

#Run tests
mocha

#Kill the webserver
kill -9 $node_pid 