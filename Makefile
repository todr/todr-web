DST_FILES = $(subst src, build, $(patsubst %.ts, %.js, $(wildcard src/*.ts) $(wildcard src/*/*.ts)))
TST_FILES = $(patsubst %.ts, %.js, $(wildcard test/*.ts) $(wildcard test/*/*.ts))

all:dependencies compile compile_test
dependencies: node_modules/package.json
compile: ROOT_DIR=src
compile: OUT_DIR=build
compile: $(DST_FILES)

compile_test:ROOT_DIR=test
compile_test:OUT_DIR=test
compile_test: $(TST_FILES)

#Compiling part
build/%.js: src/%.ts
	tsc --module commonjs --rootDir $(ROOT_DIR) --outDir $(OUT_DIR) $<

test/%.js: test/%.ts
	tsc --module commonjs --rootDir $(ROOT_DIR) --outDir $(OUT_DIR) $<

#Dependency loading part
node_modules/package.json: 	package.json
	npm install --no-bin-link
	cp package.json node_modules/package.json

#clean part
clean:
	rm -rf build
	rm -rf test/*.js

#run tests
	./run-tests.sh