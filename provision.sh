#!/usr/bin/env bash

# Provision file used by vagrant to bootstrap the vm

# apt get
# -------
echo "Apt-get update"
apt-get update &> /dev/null
echo "Setup custom ppa repositories"
apt-get install -y python-software-properties &> /dev/null
add-apt-repository -y ppa:chris-lea/node.js  &> /dev/null
echo "Apt-get update"
apt-get update &> /dev/null

echo "Installing Curl, Git and Make"
apt-get install -y build-essential &> /dev/null
apt-get install -y curl  &> /dev/null
apt-get install -y git   &> /dev/null

echo "Installing Nodejs"
apt-get install -y nodejs &> /dev/null

echo "Installing Typescript compiler"
npm install -g typescript &> /dev/null

echo "Installing RHC"
#gem install rhc &> /dev/null
