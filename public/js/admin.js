// declare a new module called 'myApp', and make it require the `ng-admin` module as a dependency
var myApp = angular.module('myApp', ['ng-admin']);
// declare a function to run when the module bootstraps (during the 'config' phase)
myApp.config(['NgAdminConfigurationProvider', function (nga) {
    // create an admin application
    var admin = nga.application('Todr Admin').baseApiUrl('http://www.thetodr.com/apiv1/55e9a51980116457e8d6c818/');

    //create a project entity
    var user = nga.entity('users');
    user.listView().fields([
        nga.field('hash'),
        nga.field('name')
    ]);

    admin.addEntity(user);
    // attach the admin application to the DOM and execute it
    nga.configure(admin);
}]);